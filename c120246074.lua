local m=120246074
local cm=_G["c"..m]
cm.name="真·兽机界霸者 狮虎王"
function cm.initial_effect(c)
	--Destroy
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(m,0))
	e1:SetCategory(CATEGORY_ATKCHANGE+CATEGORY_POSITION)
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCost(cm.cost)
	e1:SetOperation(cm.operation)
	c:RegisterEffect(e1)
end
--Destroy
function cm.costfilter(c)
	return c:IsFaceup() and c:IsRace(RACE_BEASTWARRIOR) and c:GetBaseAttack()>0 and c:IsAbleToGraveAsCost()
end
function cm.posfilter(c)
	return RD.IsCanChangePosition(c)
end
cm.cost=RD.CostSendMZoneToGrave(cm.costfilter,1,1,true,function(g)
	return g:GetSum(Card.GetBaseAttack)
end)
function cm.operation(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if c:IsFaceup() and c:IsRelateToEffect(e) then
		local atk=e:GetLabel()
		RD.AttachAtkDef(e,c,atk,0,RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END)
		if Duel.GetFieldGroupCount(tp,LOCATION_FZONE,0)>0 then
			RD.CanSelectAndDoAction(aux.Stringid(m,3),HINTMSG_POSCHANGE,cm.posfilter,tp,0,LOCATION_MZONE,1,1,nil,function(sg)
				RD.ChangePosition(sg)
			end)
		end
	end
end