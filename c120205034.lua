local m=120205034
local list={120140028,120130023}
local cm=_G["c"..m]
cm.name="无限之浪漫式闪电吉他"
function cm.initial_effect(c)
	RD.AddCodeList(c,list)
	--Fusion Material
	RD.AddFusionProcedure(c,list[1],list[2])
	--Multi-Choose Effect
	RD.CreateMultiChooseEffect(c,nil,cm.cost,aux.Stringid(m,1),cm.target1,cm.operation1,aux.Stringid(m,2),cm.target2,cm.operation2,CATEGORY_DAMAGE,CATEGORY_DRAW+CATEGORY_TOGRAVE)
end
--Multi-Choose Effect
function cm.costfilter(c)
	return c:IsCode(list[1],list[2]) and c:IsAbleToDeckOrExtraAsCost()
end
cm.cost=RD.CostSendGraveSubToDeck(cm.costfilter,aux.dncheck,2,2)
--Damage
function cm.target1(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetOperationInfo(0,CATEGORY_DAMAGE,nil,0,1-tp,1500)
end
function cm.operation1(e,tp,eg,ep,ev,re,r,rp)
	Duel.Damage(1-tp,1500,REASON_EFFECT)
end
--Draw
function cm.target2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsPlayerCanDraw(tp,3) end
	Duel.SetOperationInfo(0,CATEGORY_DRAW,nil,0,tp,3)
	Duel.SetOperationInfo(0,CATEGORY_TOGRAVE,nil,2,tp,LOCATION_HAND)
end
function cm.operation2(e,tp,eg,ep,ev,re,r,rp)
	if Duel.Draw(tp,3,REASON_EFFECT)~=0 then
		RD.SelectAndDoAction(HINTMSG_TOGRAVE,Card.IsAbleToGrave,tp,LOCATION_HAND,0,2,2,nil,function(g)
			Duel.BreakEffect()
			RD.SendToGraveAndExists(g)
		end)
	end
end